# Dotfiles

THIS IS VERY MUCH A WORK IN PROGRESS AND IS NOT TESTED COMPLETELY. IT PROBABLY DOESN'T WORK YET.

## Initial Setup

### Installation

First, install XCode CLI tools. To do so, open a terminal and type:

```bash
xcode-select --install
```

Then, clone the dotfiles repository to your computer. You can place it anywhere and symlinks will be created to reference it from the directory.

```bash
git clone https://github.com/jeremybise/dotfiles.git ~/.dotfiles
```

Run the setup script:

```bash
cd ~/.dotfiles
./install.sh
```

The script will:

1. ...
2. ...

*TODO: Will expand on this later...*
