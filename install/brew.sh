#!/bin/sh

if test! $(which brew); then
	echo "Installing homebrew"
	ruby -e "$(curl -fsSL
	https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

echo -e "\n\nInstalling homebrew packages..."

# cli tools

# dev server setup

# dev tools
brew install git
brew install macvim --override-system-vim
brew install tmux
brew install reattach-to-user-namespace
brew install zsh
brew install highlight
brew install markdown
brew install zsh-syntax-highlighting
brew install zsh-autosuggestions
brew install tmate

exit 0
